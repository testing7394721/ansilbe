#!/usr/bin/env python
import json
import subprocess

def get_all_regions():
    cmd = "aws ec2 describe-regions --query 'Regions[*].RegionName' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def get_ec2_instances(region):
    cmd = f"aws ec2 describe-instances --region {region} --query 'Reservations[*].Instances[*].[PublicIpAddress,PrivateIpAddress,KeyName,Tags[?Key==`Name`].Value|[0],State.Name]' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def main():
    # Prompt the user for the group names
    public_group_name = input("Enter the group name for public instances: ")
    private_group_name = input("Enter the group name for private instances: ")

    all_regions = get_all_regions()

    # Write the group names to the files only once
    with open("public_inventory.txt", "w") as public_file:
        public_file.write(f"[{public_group_name}]\n")

    with open("private_inventory.txt", "w") as private_file:
        private_file.write(f"[{private_group_name}]\n")

        for region in all_regions:
            instances = get_ec2_instances(region)
            for instance_group in instances:
                for instance in instance_group:
                    public_ip, private_ip, key_name, name, state = instance[:5]

                    if state.lower() == 'running':
                        if public_ip:
                            with open("public_inventory.txt", "a") as public_file:
                                public_file.write(f"{name} ansible_host={public_ip} ansible_user=ubuntu ansible_ssh_private_key_file=/home/sandeepabbadi/Downloads/{key_name}.pem ansible_ssh_common_args='-o StrictHostKeyChecking=no'\n")
                        elif private_ip:
                            with open("private_inventory.txt", "a") as private_file:
                                private_file.write(f"{name} ansible_host={private_ip} ansible_user=ubuntu ansible_ssh_private_key_file=/home/sandeepabbadi/Downloads/{key_name}.pem ansible_ssh_common_args='-o StrictHostKeyChecking=no'\n")

if __name__ == "__main__":
    main()

