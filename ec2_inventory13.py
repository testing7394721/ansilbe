#!/usr/bin/env python
import json
import subprocess

def get_all_regions():
    cmd = "aws ec2 describe-regions --query 'Regions[*].RegionName' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def get_ec2_instances(region):
    cmd = f"aws ec2 describe-instances --region {region} --query 'Reservations[*].Instances[*].[InstanceId,PublicIpAddress,PrivateIpAddress,Tags[?Key==`Name`].Value|[0],Tags[?Key==`ubuntu_version`].Value|[0],KeyName]' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def create_inventory(instances, is_public=True):
    inventory = ""

    for instance_group in instances:
        if not isinstance(instance_group, list):
            print(f"Error: Unexpected instance data format - {instance_group}")
            continue

        for instance in instance_group:
            # Ensure that 'instance' is a list
            if not isinstance(instance, list):
                print(f"Error: Unexpected instance data format - {instance}")
                continue

            instance_id, public_ip, private_ip, name, key_name = instance[:5]  # Extract the first 5 values

            ubuntu_version = None
            if len(instance) >= 6 and instance[5] is not None:
                ubuntu_version = instance[5]

            pem_file_path = f"/home/sandeepabbadi/Downloads/{key_name}.pem"

            # Include instances with valid public IP addresses in the public inventory
            if is_public and public_ip is not None:
                inventory += f"{name} ansible_host={public_ip} ansible_user=ubuntu ansible_ssh_private_key_file={pem_file_path} ansible_ssh_common_args='-o StrictHostKeyChecking=no'\n"
                inventory_file = "public_inventory.txt"
            # Include instances with private IP addresses in the private inventory
            elif not is_public and private_ip is not None:
                inventory += f"{name} ansible_host={private_ip} ansible_user=ubuntu ansible_ssh_private_key_file={pem_file_path} ansible_ssh_common_args='-o StrictHostKeyChecking=no'\n"
                inventory_file = "private_inventory.txt"
            
            # Append to the respective inventory file
            with open(inventory_file, "a") as file:
                file.write(f"{name} ansible_host={public_ip if is_public else private_ip} ansible_user=ubuntu ansible_ssh_private_key_file={pem_file_path} ansible_ssh_common_args='-o StrictHostKeyChecking=no'\n")

    return inventory

def main():
    regions = get_all_regions()
    
    for region in regions:
        instances = get_ec2_instances(region)
        public_inventory = create_inventory(instances, is_public=True)
        private_inventory = create_inventory(instances, is_public=False)

        print(f"Public Inventory for region {region}:")
        print(public_inventory)
        print("\n" + "-"*50 + "\n")

        print(f"Private Inventory for region {region}:")
        print(private_inventory)
        print("\n" + "-"*50 + "\n")

if __name__ == "__main__":
    main()

