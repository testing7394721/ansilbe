#!/usr/bin/env python
import json
import subprocess

def get_ec2_instances():
    cmd = "aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId,PublicIpAddress,PrivateIpAddress,Tags[?Key==`Name`].Value|[0],Tags[?Key==`ubuntu_version`].Value|[0]]' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def create_inventory(instances):
    print(json.dumps(instances, indent=2))  # Print the instances list for debugging

    inventory = {}

    for instance in instances:
        # Ensure that 'instance' is a list with the expected structure
        if len(instance) < 5:
            print(f"Error: Unexpected instance data format - {instance}")
            continue

        instance_id, public_ip, private_ip, name, ubuntu_version = instance
        if not ubuntu_version:
            continue

        group_name = f"ubuntu{ubuntu_version}"
        if group_name not in inventory:
            inventory[group_name] = {"hosts": []}

        inventory[group_name]["hosts"].append(public_ip)

    return inventory

def main():
    instances = get_ec2_instances()
    inventory = create_inventory(instances)
    print(json.dumps(inventory, indent=2))

if __name__ == "__main__":
    main()

