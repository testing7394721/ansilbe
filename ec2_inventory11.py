#!/usr/bin/env python
import json
import subprocess
import os

def get_all_regions():
    cmd = "aws ec2 describe-regions --query 'Regions[*].RegionName' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def get_ec2_instances(region):
    cmd = f"aws ec2 describe-instances --region {region} --query 'Reservations[*].Instances[*].[InstanceId,PublicIpAddress,PrivateIpAddress,KeyName,Tags[?Key==`Name`].Value|[0],Tags[?Key==`ubuntu_version`].Value|[0]]' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def get_key_pair_name(region, key_name):
    cmd = f"aws ec2 describe-key-pairs --region {region} --key-names {key_name} --query 'KeyPairs[0].KeyName' --output text"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return result.stdout.strip()
    else:
        print(f"Error fetching key pair name: {result.stderr}")
        return None

def main():
    regions = get_all_regions()

    public_instances = []
    private_instances = []

    for region in regions:
        instances = get_ec2_instances(region)

        for instance_group in instances:
            for instance in instance_group:
                instance_id, public_ip, private_ip, key_name, name = instance[:5]

                key_pair_name = get_key_pair_name(region, key_name)

                if key_pair_name:
                    private_key_file_path = f"/home/sandeepabbadi/Downloads/{key_pair_name}.pem"

                    if not os.path.exists(private_key_file_path):
                        cmd = f"aws ec2 describe-key-pairs --region {region} --key-names {key_pair_name} --query 'KeyPairs[0].KeyMaterial' --output text"
                        key_material = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
                        
                        if key_material.returncode == 0:
                            with open(private_key_file_path, "w") as private_key_file:
                                private_key_file.write(key_material.stdout)

                            os.chmod(private_key_file_path, 0o600)  # Set proper permissions on the key file

                            if public_ip:
                                public_instances.append((public_ip, private_key_file_path))
                            elif private_ip:
                                private_instances.append((private_ip, private_key_file_path))
                        else:
                            print(f"Error fetching key material: {key_material.stderr}")

    with open("public_inventory.txt", "w") as public_file:
        for ip, key_file in public_instances:
            public_file.write(f"{ip} ansible_user=ubuntu ansible_ssh_private_key_file={key_file} ansible_ssh_common_args='-o StrictHostKeyChecking=no'\n")

    with open("private_inventory.txt", "w") as private_file:
        for ip, key_file in private_instances:
            private_file.write(f"{ip} ansible_user=ubuntu ansible_ssh_private_key_file={key_file} ansible_ssh_common_args='-o StrictHostKeyChecking=no'\n")

if __name__ == "__main__":
    main()

