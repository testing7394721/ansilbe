#!/usr/bin/env python
import json
import subprocess

def get_all_regions():
    cmd = "aws ec2 describe-regions --query 'Regions[*].RegionName' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def get_ec2_instances(region):
    cmd = f"aws ec2 describe-instances --region {region} --query 'Reservations[*].Instances[*].[InstanceId,PublicIpAddress,PrivateIpAddress,Tags[?Key==`Name`].Value|[0],Tags[?Key==`ubuntu_version`].Value|[0]]' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def get_pem_file(region, instance_id, key_name):
    if key_name is None:
        print(f"Error: Key name not found for instance {instance_id}")
        return None

    cmd = f"aws ec2 get-password-data --region {region} --instance-id {instance_id} --priv-launch-key {key_name}"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    if result.returncode == 0:
        return result.stdout
    else:
        print(f"Error fetching PEM file: {result.stderr}")
        return None

def create_inventory(instances):
    print(json.dumps(instances, indent=2))  # Print the instances list for debugging

    inventory = {}

    for instance_group in instances:
        if not isinstance(instance_group, list):
            print(f"Error: Unexpected instance data format - {instance_group}")
            continue

        for instance in instance_group:
            # Ensure that 'instance' is a list
            if not isinstance(instance, list):
                print(f"Error: Unexpected instance data format - {instance}")
                continue

            instance_id, public_ip, private_ip, name = instance[:4]  # Extract the first 4 values
            ubuntu_version = None

            if len(instance) >= 5 and instance[4] is not None:
                ubuntu_version = instance[4]

            pem_file = get_pem_file(region, instance_id, name)

            if ubuntu_version is not None:
                group_name = f"ubuntu{ubuntu_version}"
                if group_name not in inventory:
                    inventory[group_name] = {"hosts": [], "vars": {"ansible_ssh_private_key_file": pem_file}}

                inventory[group_name]["hosts"].append(public_ip)

    return inventory

def main():
    regions = get_all_regions()
    
    for region in regions:
        instances = get_ec2_instances(region)
        inventory = create_inventory(instances)
        print(f"Inventory for region {region}:")
        print(json.dumps(inventory, indent=2))
        print("\n" + "-"*50 + "\n")

if __name__ == "__main__":
    main()

