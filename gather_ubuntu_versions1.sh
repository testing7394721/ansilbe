#!/bin/bash
echo "[2345678-panini]" > "inventory.ini" #it should be replaced based on your accountnumber-projectname

# Concatenate public inventory to the main inventory file
cat public_inventory.txt >> inventory.ini

# Add an empty line
echo >> inventory.ini

# Concatenate private inventory to the main inventory file
cat private_inventory.txt >> inventory.ini

#echo "[2345678-panini]" > "inventory.ini"

#cat public_inventory.txt <(echo) private_inventory.txt > inventory.ini

# Run Ansible playbook to check Ubuntu versions
ansible-playbook -i inventory.ini check_ubuntu_versions.aws_ec2.yml

# Create a directory to store Ubuntu version directories
mkdir -p ubuntu_versions

# Read lines from ubuntu_versions file and loop through each line
while IFS= read -r line; do
    # Extract hostname and version from the line
    host=$(echo "$line" | cut -d'=' -f1)
    version=$(echo "$line" | cut -d'=' -f2)

    # Create a directory for the Ubuntu version
    mkdir -p "ubuntu_versions/$version"

    # Check if the section header already exists in the inventory file for the specific Ubuntu version
    if ! grep -q "\[2345678-panini]" "ubuntu_versions/$version/inventory.ini"; then
        # Append the section header to the inventory file
        echo "[2345678-panini]" >> "ubuntu_versions/$version/inventory.ini"
    fi

    # Append the host information to the inventory file
    grep "^$host " inventory.ini >> "ubuntu_versions/$version/inventory.ini"

    # Copy group vars to ubuntu-versions
    cp -r group_vars/ "ubuntu_versions/$version"
   

done < ./ubuntu-versions

