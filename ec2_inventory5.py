import boto3

def extract_ubuntu_version(ami_name):
    # Implement logic to extract Ubuntu version from the AMI name
    # For example, you can use regular expressions or string manipulation
    # For simplicity, assuming that the Ubuntu version is in the format 'ubuntu-XX.XX'
    if 'ubuntu' in ami_name.lower():
        return ami_name.lower().split('ubuntu-')[1].split('-')[0]
    else:
        return 'unknown'

def create_inventory(instances):
    inventory = {"all": {"hosts": {}}, "_meta": {"hostvars": {}}}

    for instance in instances:
        instance_id, public_ip, private_ip, name, ami_name = instance
        ubuntu_version = extract_ubuntu_version(ami_name)

        inventory["all"]["hosts"][instance_id] = {
            "public_ip": public_ip,
            "private_ip": private_ip,
            "name": name,
            "ubuntu_version": ubuntu_version,
        }

    return inventory

def main():
    ec2 = boto3.resource('ec2')
    instances = []

    for instance in ec2.instances.all():
        instance_id = instance.id
        public_ip = instance.public_ip_address
        private_ip = instance.private_ip_address
        name = instance.tags[0]['Value'] if instance.tags else 'unknown'
        ami_name = instance.image.name if instance.image.name else 'unknown'
        instances.append([instance_id, public_ip, private_ip, name, ami_name])

    inventory = create_inventory(instances)
    print(inventory)

if __name__ == "__main__":
    main()

