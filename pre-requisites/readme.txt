before we setup and write ansible and it's playbooks we have to setup below pre-requisites 

1. install ansible
2. create a user for anisible

adduser ansible
Enter password: A1n$s!e@123
usermod -aG sudo ansible
usermod -aG root ansible
usermod -aG ubuntu ansible
visudo
Add the following in sudo file
ansible ALL=(ALL) NOPASSWD: ALL
service ssh restart

In ansible master:
ssh-copy-id ansible@ip_address

3. for dynamic inventory ,we use python 

install python and required dependencies ex- boto3 


4. place your pemfiles in /home/ansible/pemfiles or /home/username/pemfiles

5. create all projects directories in ansible_source_directory, in my case source directory is /home/ansible/ansible/aws

cd /home/ansible/ansible/aws

create your project related directories ( project name should be same as the directory name)

and copy your required files to all projects related directories ex- dynamic_inventory.sh and ec2_dynamic_inventory.py and ecs_ec2_dynamic_inventory.py 

6. for connecting private machines through jump host , create a group_vars in that we should create a file ,file name should be same as inventory group name 

#group_vars/xcubelabs.yml

ansible_ssh_common_args: '-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ProxyCommand="ssh -W %h:%p -q xcubelabs"'




