#!/usr/bin/env python
import json
import subprocess

def get_ec2_instances():
    cmd = "aws ec2 describe-instances --query 'Reservations[*].Instances[*].[PublicIpAddress,PrivateIpAddress,KeyName,Tags[?Key==`Name`].Value|[0]]' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def main():
    instances = get_ec2_instances()

    public_instances = []
    private_instances = []

    for instance_group in instances:
        for instance in instance_group:
            public_ip, private_ip, key_name, name = instance[:4]

            if public_ip:
                public_instances.append((public_ip, key_name))
            elif private_ip:
                private_instances.append((private_ip, key_name))

    with open("public_inventory.txt", "w") as public_file:
        for ip, key_name in public_instances:
            public_file.write(f"{ip} ansible_user=ubuntu ansible_ssh_private_key_file={key_name}.pem ansible_ssh_common_args='-o StrictHostKeyChecking=no'\n")

    with open("private_inventory.txt", "w") as private_file:
        for ip, key_name in private_instances:
            private_file.write(f"{ip} ansible_user=ubuntu ansible_ssh_private_key_file={key_name}.pem ansible_ssh_common_args='-o StrictHostKeyChecking=no'\n")

if __name__ == "__main__":
    main()

