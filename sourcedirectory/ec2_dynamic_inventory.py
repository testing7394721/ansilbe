#!/usr/bin/env python
import json
import subprocess

def get_all_regions():
    cmd = "aws ec2 describe-regions --query 'Regions[*].RegionName' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def get_ec2_instances(region):
    cmd = f"aws ec2 describe-instances --region {region} --query 'Reservations[*].Instances[*].[PublicIpAddress,PrivateIpAddress,KeyName,Tags[?Key==`Name`].Value|[0],State.Name]' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def main():
    all_regions = get_all_regions()

    with open("public_inventory.ini", "w") as public_file, open("private_inventory.ini", "w") as private_file:
        for region in all_regions:
            instances = get_ec2_instances(region)

            for instance_group in instances:
                for instance in instance_group:
                    public_ip, private_ip, key_name, name, state = instance[:5]

                    # Check if name is not None and "ecs" is not in the lowercased name and "ECS" is not in the uppercased name
                    if name and "ecs" not in name.lower() and "ECS" not in name.upper():
                        if state.lower() == 'running':
                            if public_ip:
                                public_file.write(f"{name} ansible_host={public_ip} ansible_user=ubuntu ansible_ssh_private_key_file=/home/ptadmin/pemfiles/{key_name}.pem ansible_ssh_common_args='-o StrictHostKeyChecking=no'\n\n")
                            elif private_ip:
                                private_file.write(f"{name} ansible_host={private_ip} ansible_user=ubuntu ansible_ssh_private_key_file=/home/ptadmin/pemfiles/{key_name}.pem\n\n")

if __name__ == "__main__":
    main()