#!/bin/bash

# Read the account number from accountnumber.txt based on the current directory name
dir_name=$(basename "$(pwd)")
account_file="/home/ptadmin/ansible/aws-wazuh/accountnumber.txt"

if [ -f "$account_file" ]; then
    account_number=$(grep "^$dir_name=" "$account_file" | cut -d'=' -f2)
    if [ -n "$account_number" ]; then
        echo "This dynamic inventory will be fetched from $dir_name-$account_number"
        aws configure set role_arn "arn:aws:iam::$account_number:role/admin"
    else
        echo "Error: Account number not found for directory $dir_name in $account_file"
        exit 1
    fi
else
    echo "Error: $account_file not found"
    exit 1
fi

python3 ec2_dynamic_inventory.py  #ec2_dynamic_inventory.py this file should be present in all directory of the project 

#python3 privateip.py

echo "[$dir_name]" > "inventory.ini" # it should be replaced based on your accountnumber

# Add an empty line
echo >> inventory.ini

# Concatenate public inventory to the main inventory file
cat public_inventory.ini >> inventory.ini

# Add an empty line
echo >> inventory.ini

# Concatenate private inventory to the main inventory file
cat private_inventory.ini >> inventory.ini
if grep -qE "(ansible_host|ansible_user|pem)" "inventory.ini"; then
    echo "Instances details are available on $dir_name account-$account_number"
else
   echo "No running instances are available on $dir_name account-$account_number"
fi