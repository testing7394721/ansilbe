#!/bin/bash

# Prompt for ansiblehostname
read -p "Enter the Ansible hostname or group name: " ansiblehostname

# Prompt for ip_address_to_block
read -p "Enter the IP address to block: " ip_address_to_block

# Run Ansible playbook with the provided inputs
ansible-playbook -i public_inventory.ini -l $ansiblehostname ip-block-nginx-apache.yml -e "ip_address_to_block=$ip_address_to_block"

