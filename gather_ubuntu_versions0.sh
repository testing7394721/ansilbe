#!/bin/bash

cat public_inventory.txt <(echo) private_inventory.txt > inventory.ini

# Run Ansible playbook to check Ubuntu versions
ansible-playbook -i inventory.ini check_ubuntu_versions.aws_ec2.yml

# Create a directory to store Ubuntu version directories
mkdir -p ubuntu_versions

# Read lines from /tmp/ubuntu_versions and loop through each line
while IFS= read -r line; do
    # Extract hostname and version from the line
    host=$(echo "$line" | cut -d'=' -f1)
    version=$(echo "$line" | cut -d'=' -f2)
  
    # Create a directory for the Ubuntu version

    mkdir -p "ubuntu_versions/$version"

    echo "[2345678-panini]" >> "ubuntu_versions/$version/inventory.ini"

    # Copy the content from your_inventory.ini to the corresponding inventory file
    grep "^$host " inventory.ini >> "ubuntu_versions/$version/inventory.ini"

    #copy group vars to ubuntu-versions

    cp -r group_vars/ "ubuntu_versions/$version"

done < ./ubuntu-versions

