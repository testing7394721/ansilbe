#!/usr/bin/env python
import json
import subprocess

def get_all_regions():
    cmd = "aws ec2 describe-regions --query 'Regions[*].RegionName' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def get_ec2_instances(region):
    cmd = f"aws ec2 describe-instances --region {region} --query 'Reservations[*].Instances[*].[InstanceId,PublicIpAddress,PrivateIpAddress,Tags[?Key==`Name`].Value|[0],ImageId]' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def extract_ubuntu_version(ami_name):
    # Extract Ubuntu version from AMI name
    parts = ami_name.split('/')
    if len(parts) >= 3 and parts[2].startswith('ubuntu-'):
        return parts[2][7:]  # Extract the version after 'ubuntu-'
    else:
        return None

def create_inventory(instances):
    print(json.dumps(instances, indent=2))  # Print the instances list for debugging

    inventory = {"ubuntu_versions": {}}

    for instance_group in instances:
        if not isinstance(instance_group, list):
            print(f"Error: Unexpected instance data format - {instance_group}")
            continue

        for instance in instance_group:
            # Ensure that 'instance' is a list
            if not isinstance(instance, list):
                print(f"Error: Unexpected instance data format - {instance}")
                continue

            instance_id, public_ip, private_ip, name, ami_id = instance + [None] * (5 - len(instance))

            # Extract Ubuntu version from AMI name
            ubuntu_version = None
            if ami_id is not None:
                ubuntu_version = extract_ubuntu_version(ami_id)

            if ubuntu_version is not None:
                if ubuntu_version not in inventory["ubuntu_versions"]:
                    inventory["ubuntu_versions"][ubuntu_version] = {"hosts": []}

                inventory["ubuntu_versions"][ubuntu_version]["hosts"].append({
                    "public_ip": public_ip,
                    "instance_id": instance_id,
                    "name": name,
                    "ubuntu_version": ubuntu_version
                })
            else:
                if "other" not in inventory:
                    inventory["other"] = {"hosts": []}

                inventory["other"]["hosts"].append({
                    "public_ip": public_ip,
                    "instance_id": instance_id,
                    "name": name,
                    "ubuntu_version": "unknown"
                })

    return inventory

def main():
    regions = get_all_regions()
    
    for region in regions:
        instances = get_ec2_instances(region)
        inventory = create_inventory(instances)
        print(f"Inventory for region {region}:")
        print(json.dumps(inventory, indent=2))
        print("\n" + "-"*50 + "\n")

if __name__ == "__main__":
    main()

