#!/usr/bin/env python
import json
import subprocess

def get_all_regions():
    cmd = "aws ec2 describe-regions --query 'Regions[*].RegionName' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def get_ec2_instances(region):
    cmd = f"aws ec2 describe-instances --region {region} --query 'Reservations[*].Instances[*].[InstanceId,PublicIpAddress,PrivateIpAddress,Tags[?Key==`Name`].Value|[0],Tags[?Key==`ubuntu_version`].Value|[0]]' --output json"
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    
    if result.returncode == 0:
        return json.loads(result.stdout)
    else:
        print(f"Error: {result.stderr}")
        return []

def create_inventory(instances):
    inventory = {}

    for instance_group in instances:
        if not isinstance(instance_group, list):
            print(f"Error: Unexpected instance data format - {instance_group}")
            continue

        for instance in instance_group:
            # Ensure that 'instance' is a list
            if not isinstance(instance, list):
                print(f"Error: Unexpected instance data format - {instance}")
                continue

            instance_id, public_ip, private_ip, name = instance[:4]  # Extract the first 4 values

            if public_ip:  # Check if it's a public instance
                inventory[name] = {
                    "ansible_host": public_ip,
                    "ansible_user": "ubuntu",
                    "ansible_ssh_private_key_file": f"/home/sandeepabbadi/Downloads/{name}.pem",
                    "ansible_ssh_common_args": "-o StrictHostKeyChecking=no"
                }
            elif private_ip:  # Check if it's a private instance
                inventory[name] = {
                    "ansible_host": private_ip,
                    "ansible_user": "ubuntu",
                    "ansible_ssh_private_key_file": f"/home/sandeepabbadi/Downloads/{name}.pem",
                    "ansible_ssh_common_args": "-o StrictHostKeyChecking=no"
                }

    return inventory

def main():
    regions = get_all_regions()
    
    for region in regions:
        instances = get_ec2_instances(region)
        inventory = create_inventory(instances)
        print(f"Inventory for region {region}:")
        print(json.dumps(inventory, indent=2))
        print("\n" + "-"*50 + "\n")

if __name__ == "__main__":
    main()

